db.createUser({
  user: "root",
  pwd: "root-secret-password",
  roles: [
    {
      role: "readWrite",
      db: "portfolio_rottigni_tech"
    }
  ]
})